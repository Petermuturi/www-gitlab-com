---
layout: job_page
title: "Developer"
---

At GitLab, developers are highly independent and self-organized individual
contributors who work together as a tight team in a [remote and agile](/2015/09/14/remote-agile-at-gitlab/) way.

Most backend developers work on all aspects of GitLab, building features, fixing bugs, and generally improving the application.
Some developers [specialize](/jobs/specialist) and focus on a specific area, such as packaging, performance or GitLab CI.
Developers can specialize immediately after joining, or after some time, when they have gained familiarity with many areas of GitLab and find one they would like to focus on.

## Responsibilities

* Develop features from proposal to polished end result.
* Support and collaborate with our [service engineers](/jobs/service-engineer) in getting to the bottom of user-reported issues and come up with robust solutions.
* Engage with the core team and the open source community to collaborate on improving GitLab.
* Manage and review code contributed by the rest of the community and work with them to get it ready for production.
* Create and maintain documentation around features and configuration to save our users time.
* Take initiative in improving the software in small or large ways to address pain points in your own experience as a developer.
* Keep code easy to maintain and keep it easy for others to contribute code to GitLab.
* Qualify developers for hiring.

## Requirements

* You can reason about software, algorithms, and performance from a high level
* You are passionate about open source
* You have worked on a production-level Ruby application, preferably using Rails. (This is a [strict requirement](#ruby-experience))
* You know how to write your own Ruby gem using TDD techniques
* Strong written communication skills
* Experience with Docker, Nginx, Go, and Linux system administration a plus
* Experience with online community development a plus
* Self-motivated and have strong organizational skills
* You share our [values](/handbook/#values), and work in accordance with those values.
* [A technical interview](/jobs/#technical-interview) is part of the hiring process for this position.

### Ruby experience

For this position, a significant amount of experience with Ruby is a **strict requirement**.

We would love to hire all great backend developers, regardless of the language they have most experience with,
but at this point we are looking for developers who can get up and running within the GitLab code base very quickly
and without requiring much training, which limits us to developers with a large amount of existing experience with Ruby, and preferably Rails too.

For a time, we also considered applicants with little or no Ruby and Rails experience for this position,
because we realize that programming skills are to a large extent transferable between programming languages,
but we are not currently doing that anymore for the reasons described in the
[merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2695)
that removed the section from this listing that described that policy.

If you think you would be an asset to our engineering team regardless, please see if [another position](/jobs) better fits your experiences and interests,
or apply using the [Open Application](/jobs/open-application/).

If you would still prefer to join the backend development team as a Ruby developer,
please consider contributing to the open-source [GitLab Community Edition](https://gitlab.com/gitlab-org/gitlab-ce).
We frequently hire people from the community who have shown through contributions that
they have the skills that we are looking for, even if they didn’t have much previous experience
with those technologies, and we would gladly review those contributions.

## Workflow

The basics of GitLab development can be found in the [developer onboarding](/handbook/developer-onboarding/#basics-of-gitlab-development) document.

The handbook details the complete [GitLab Workflow](/handbook/#gitlab-workflow).

## Senior Developers

Senior Developers are experienced developers who meet the following criteria:

1. Technical Skills
    * Are able to write modular, well-tested, and maintainable code
    * Know a domain really well and radiate that knowledge
2. Leadership
    * Begins to show architectural perspective
    * Leads the design for medium to large projects with feedback from other engineers
3. Code quality
    * Leaves code in substantially better shape than before
    * Fixes bugs/regressions quickly
    * Monitors overall code quality/build failures
    * Creates test plans
4. Communication
    * Provides thorough and timely code feedback for peers
    * Able to communicate clearly on technical topics
    * Keeps issues up-to-date with progress
    * Helps guide other merge requests to completion
    * Helps with recruiting

## Staff Developers

A Senior Developer will be promoted to a Staff Developer when he/she has
demonstrated significant leadership to deliver high-impact projects. This may
involve a number of items:

1. Proposing new ideas, performing feasibility analyses and scoping the work
2. Leading the design for large projects
3. Working across functional groups to deliver the project
4. Proactively identifying and reducing technical debt
5. Writing in-depth documentation that shares knowledge and radiates GitLab technical strengths
6. The ability to create innovative solutions that push GitLab's technical abilities ahead of the curve
7. Identifies significant projects that result in substantial cost savings or revenue
8. Proactively defines and solves important architectural issues
9. TODO

## Internships

We normally don't offer any internships, but if you get a couple of merge requests
accepted, we'll interview you for one. This will be a remote internship without
supervision; you'll only get feedback on your merge requests. If you want to
work on open source and qualify please [submit an application](https://gitlab.workable.com/jobs/106660/candidates/new).
In the cover letter field, please note that you want an internship and link to
the accepted merge requests. The merge requests should be of significant
value and difficulty, which is at the discretion of the hiring manager. For
example, fixing 10 typos isn't as valuable as shipping 2 new features.

## Relevant links

- [Engineering Handbook](/handbook/engineering)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).


* Part of the application process is to provide links to some code. Some candidates want to share code privately, so if they do, we send them an email with our GitLab.com / GitHub / Bitbucket usernames and also invite them to email code to us.
* If the code looks good, we ask the candidate to answer two questions about web applications. One is fairly Rails-specific, to do with the MVC pattern, concerns, and services; the other is about the stages a web request goes through when someone gets the GitLab CE repo on GitLab.com.
* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first 15 minute soft-skills interview with a Discussion/ Platform Lead
* Candidates will then be invited to schedule a technical interview with the Platform/ Discussion Lead
* Candidates will be invited to schedule a third one hour interview with our VP of Engineering
* Finally, candidates will have a 50 minute interview with our CEO
* Successful candidates will subsequently be made an offer via email


Additional details about our process can be found on our [hiring page](/handbook/hiring).
